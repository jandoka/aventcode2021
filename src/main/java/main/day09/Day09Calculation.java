package main.day09;

import main.AdventDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Day09Calculation implements AdventDay {

    public void calculate1(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

        }
    }

    public void calculate2(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

        }
    }
}