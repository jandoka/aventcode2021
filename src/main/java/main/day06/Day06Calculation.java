package main.day06;

import main.AdventDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

public class Day06Calculation implements AdventDay {

    public void calculate1(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = reader.readLine();
            List<Lanternfish> fishList = Arrays.stream(line.split(",")).map(c -> new Lanternfish(Integer.valueOf(c))).collect(Collectors.toList());
            ConcurrentLinkedQueue<Lanternfish> fishes = new ConcurrentLinkedQueue<>(fishList);
//            System.out.print("init state: ");
//            System.out.println(fishes.stream().map(f -> f.getValue()).collect(Collectors.toList()));

            for (int day = 1; day <= 18; day++) {
                Iterator<Lanternfish> fishIterator = fishes.iterator();
                while (fishIterator.hasNext()) {
                    Lanternfish lanternfish = fishIterator.next();
                    if (lanternfish.getValue() == 0) {
                        fishes.add(new Lanternfish(9));
                        lanternfish.setValue(6);
                    } else {
                        lanternfish.minus();
                    }
                }
//                System.out.print("after day " + day + " ");
//                System.out.println(fishes.stream().map(f -> f.getValue()).collect(Collectors.toList()));
            }
            System.out.println("Result: " + fishes.size());
        }
    }

    public void calculate2(String file) throws IOException {
        final int NUM_OF_DAYS = 256;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = reader.readLine();
            List<Integer> fishList = Arrays.stream(line.split(",")).map(c -> (Integer.valueOf(c))).collect(Collectors.toList());
            ConcurrentHashMap<Integer, Long> valueWithNumOfFish = new ConcurrentHashMap<>();
            for (int i = 0; i < 9; i++) {
                valueWithNumOfFish.put(i, 0L);
            }
            fishList.forEach(fishValue -> {
                long actualFishValue = valueWithNumOfFish.getOrDefault(fishValue, 0L);
                valueWithNumOfFish.put(fishValue, actualFishValue + 1);
            });


            for (int day = 1; day <= NUM_OF_DAYS; day++) {
                long tmp0 = valueWithNumOfFish.get(0);
                for (int i = 1; i <= 8; i++) {
                    long actualValue = valueWithNumOfFish.get(i);
                    valueWithNumOfFish.put(i - 1, actualValue);
                }
                long sum6 = valueWithNumOfFish.get(6) + tmp0;
                valueWithNumOfFish.put(6, sum6);
                valueWithNumOfFish.put(8, tmp0);
            }

            long numOfFishes = valueWithNumOfFish.values().stream().mapToLong(v -> v).sum();
            System.out.println("Result: " + numOfFishes);
        }
    }
}