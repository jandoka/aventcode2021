package main.day06;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Lanternfish {
    private int value = 0;
    private boolean alreadyCreatedNewOne = false;

    public Lanternfish(int value) {
        this.value = value;
    }

    public void minus() {
        this.value -= 1;
    }
}
