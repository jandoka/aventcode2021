package main.day08;

import main.AdventDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day08Calculation implements AdventDay {

    private List<String> numbers = new ArrayList<>();

    public Day08Calculation() {
        for (int i = 0; i < 10; i++) {
            numbers.add(0, "abcefg");
            numbers.add(1, "cf");
            numbers.add(2, "acdeg");
            numbers.add(3, "acdfg");
            numbers.add(4, "bcdf");
            numbers.add(5, "abdfg");
            numbers.add(6, "abdefg");
            numbers.add(7, "acf");
            numbers.add(8, "abcdefg");
            numbers.add(9, "abcdfg");
        }
    }

    public void calculate1(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String readLine = reader.readLine();
            long count = 0;
            while (readLine != null) {
                String[] line = readLine.split(" \\| ");
                count += Arrays.stream(line[1].split(" "))
                        .filter(w -> w.length() == 2 || w.length() == 4 || w.length() == 3 || w.length() == 7)
                        .count();
                readLine = reader.readLine();
            }
            System.out.println("result: " + count);
        }
    }

    public void calculate2(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String readLine = reader.readLine();
            long sum = 0;
            while (readLine != null) {
                String[] line = readLine.split(" \\| ");
                List<String> input = Arrays.stream(line[0].split(" ")).collect(Collectors.toList());
                Map<Character, Character> resultMap = getResultMap(input);
                String[] output = line[1].split(" ");
                List<Integer> finalNumbers = getFinalNumbers(resultMap, output);
                StringBuilder sb = new StringBuilder();
                finalNumbers.forEach(sb::append);
                String resultString = sb.toString();
                Integer lineResultNumber = Integer.valueOf(resultString);
                sum += lineResultNumber;
                readLine = reader.readLine();
            }
            System.out.println("sum: " + sum);
        }
    }

    private List<Integer> getFinalNumbers(Map<Character, Character> resultMap, String[] output) {
        List<Integer> result = new ArrayList<>();
        StringBuilder subResultsInString = new StringBuilder();
        for (int i = 0; i < output.length; i++) {
            char[] chars = output[i].toCharArray();
            List<Character> correctCharsPerNumber = output[i].chars()
                    .mapToObj(c -> resultMap.get((char) c))
                    .collect(Collectors.toList());
            String correctWordPerNumber = output[i].chars()
                    .mapToObj(c -> resultMap.get((char) c))
                    .sorted()
                    .collect(Collector.of(
                            StringBuilder::new,
                            StringBuilder::append,
                            StringBuilder::append,
                            StringBuilder::toString));
            result.add(numbers.indexOf(correctWordPerNumber));
        }
        return result;
    }

    private Map<Character, Character> getResultMap(List<String> input) {
        Map<Character, List<Character>> charactersMapTmp = new HashMap<>();
        Map<Character, Character> resultCharMap = new HashMap<>();
        List<String> assignedWords = input.stream().map(w -> "").collect(Collectors.toList());
        assignedWords.set(1, input.stream().filter(w -> w.length() == 2).findAny().get());
        assignedWords.set(4, input.stream().filter(w -> w.length() == 4).findAny().get());
        assignedWords.set(7, input.stream().filter(w -> w.length() == 3).findAny().get());
        assignedWords.set(8, input.stream().filter(w -> w.length() == 7).findAny().get());

        assignWords1478(charactersMapTmp, assignedWords);
        assignRestWords(input, charactersMapTmp, assignedWords);
        assignResultMap(charactersMapTmp, resultCharMap, assignedWords);
        return resultCharMap;
    }

    private void assignResultMap(Map<Character, List<Character>> charactersMapTmp, Map<Character, Character> resultCharMap, List<String> assignedWords) {
        resultCharMap.put(charactersMapTmp.get('a').get(0), 'a');

        String word2 = assignedWords.get(2);
        Character ch2first = charactersMapTmp.get('c').get(0);
        Character ch2second = charactersMapTmp.get('c').get(1);
        if (word2.indexOf(ch2first) != -1) {
            resultCharMap.put(ch2first, 'c');
            resultCharMap.put(ch2second, 'f');
        } else {
            resultCharMap.put(ch2second, 'c');
            resultCharMap.put(ch2first, 'f');
        }
        String word3 = assignedWords.get(3);
        Character ch3first = charactersMapTmp.get('b').get(0);
        Character ch3second = charactersMapTmp.get('b').get(1);
        if (word3.indexOf(ch3first) != -1) {
            resultCharMap.put(ch3first, 'd');
            resultCharMap.put(ch3second, 'b');
        } else {
            resultCharMap.put(ch3second, 'd');
            resultCharMap.put(ch3first, 'b');
        }
        Character ch33first = charactersMapTmp.get('g').get(0);
        Character ch33second = charactersMapTmp.get('g').get(1);
        if (word3.indexOf(ch33first) != -1) {
            resultCharMap.put(ch33first, 'g');
            resultCharMap.put(ch33second, 'e');
        } else {
            resultCharMap.put(ch33second, 'g');
            resultCharMap.put(ch33first, 'e');
        }
    }

    private void assignRestWords(List<String> input, Map<Character, List<Character>> charactersMapTmp, List<String> assignedWords) {
        List<String> wordsOfNums235 = input.stream().filter(w -> w.length() == 5).collect(Collectors.toList());
        assignedWords.set(2, wordsOfNums235.stream().filter(w -> checkIfContainsPossibleChar(charactersMapTmp, w, 'e')).findAny().get());
        assignedWords.set(3, wordsOfNums235.stream().filter(w -> checkIfContainsPossibleChar(charactersMapTmp, w, 'c')).findAny().get());
        assignedWords.set(5, wordsOfNums235.stream().filter(w -> checkIfContainsPossibleChar(charactersMapTmp, w, 'b')).findAny().get());
        List<String> wordsOfNums069 = input.stream().filter(w -> w.length() == 6).collect(Collectors.toList());
        assignedWords.set(0, wordsOfNums069.stream().filter(w -> checkIfContainsPossible2Chars(charactersMapTmp, w, 'c', 'e')).findAny().get());
        assignedWords.set(6, wordsOfNums069.stream().filter(w -> checkIfContainsPossible2Chars(charactersMapTmp, w, 'b', 'e')).findAny().get());
        assignedWords.set(9, wordsOfNums069.stream().filter(w -> checkIfContainsPossible2Chars(charactersMapTmp, w, 'b', 'c')).findAny().get());
    }

    private boolean checkIfContainsPossibleChar(Map<Character, List<Character>> charactersMapTmp, String w, Character possibleChar) {
        return w.indexOf(charactersMapTmp.get(possibleChar).get(0)) != -1 && w.indexOf(charactersMapTmp.get(possibleChar).get(1)) != -1;
    }

    private boolean checkIfContainsPossible2Chars(Map<Character, List<Character>> charactersMapTmp, String w,
                                                  Character possibleChar1, Character possibleChar2) {
        return w.indexOf(charactersMapTmp.get(possibleChar1).get(0)) != -1
                && w.indexOf(charactersMapTmp.get(possibleChar1).get(1)) != -1
                && w.indexOf(charactersMapTmp.get(possibleChar2).get(0)) != -1
                && w.indexOf(charactersMapTmp.get(possibleChar2).get(1)) != -1;
    }

    private void assignWords1478(Map<Character, List<Character>> charactersMapTmp, List<String> assignedWords) {
        List<Character> assignedChars = new ArrayList<>();
        List<Character> complementChars17 = getComplementChars(assignedWords.get(1), assignedWords.get(7));
        charactersMapTmp.put('a', complementChars17);
        assignedChars.addAll(complementChars17);
        List<Character> complementChars14 = getComplementChars(assignedWords.get(1), assignedWords.get(4));
        charactersMapTmp.put('b', complementChars14);
        charactersMapTmp.put('d', complementChars14);
        assignedChars.addAll(complementChars14);
        String word7plus4 = assignedWords.get(7) + assignedWords.get(4);
        List<Character> complementChars478 = getComplementChars(word7plus4, assignedWords.get(8));
        charactersMapTmp.put('e', complementChars478);
        charactersMapTmp.put('g', complementChars478);
        assignedChars.addAll(complementChars478);
        List<Character> restChars = Stream.of('a', 'b', 'c', 'd', 'e', 'f', 'g').filter(ch -> !assignedChars.contains(ch)).collect(Collectors.toList());
        charactersMapTmp.put('c', restChars);
        charactersMapTmp.put('f', restChars);
    }

    private List<Character> getComplementChars(String a, String b) {
        Set<Character> charsSet = new HashSet<Character>();
        for (int i = 0; i < a.length(); i++) {
            char ch = a.charAt(i);
            if (b.indexOf(ch) == -1) {
                charsSet.add(Character.valueOf(ch));
            }
        }
        for (int i = 0; i < b.length(); i++) {
            char ch = b.charAt(i);
            if (a.indexOf(ch) == -1) {
                charsSet.add(Character.valueOf(ch));
            }
        }
        return new ArrayList<>(charsSet).stream().sorted().collect(Collectors.toList());
    }

    private List<Character> getCommonChars(String a, String b) {
        Set<Character> charsSet = new HashSet<Character>();
        for (int i = 0; i < a.length(); i++) {
            char ch = a.charAt(i);
            if (b.indexOf(ch) != -1) {
                charsSet.add(Character.valueOf(ch));
            }
        }
        return new ArrayList<>(charsSet);
    }
}