package main.day07;

import main.AdventDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day07Calculation implements AdventDay {

    private int maximumFrequency = 0;
    private int indexOfMaximumFrequency = 0;
    private int maxNumber = 0;

    public void calculate1(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            List<Integer> input = Arrays.stream(reader.readLine().split(",")).map(Integer::valueOf).collect(Collectors.toList());
            maxNumber = input.stream().mapToInt(x -> x).max().getAsInt();
            Map<Integer, Integer> frequency = countFreq(input);
            int dist = tryToFindMinDist(input, frequency);
            System.out.println("result: " + dist);
        }
    }

    public void calculate2(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            List<Integer> input = Arrays.stream(reader.readLine().split(",")).map(Integer::valueOf).collect(Collectors.toList());
            maxNumber = input.stream().mapToInt(x -> x).max().getAsInt();
            Map<Integer, Integer> frequency = countFreq(input);
            int dist = tryToFindMinDist2(input, frequency);
            System.out.println("result: " + dist);
        }
    }

    private int tryToFindMinDist2(List<Integer> input, Map<Integer, Integer> frequency) {
//        int startPoint = indexOfMaximumFrequency;
        int startPoint = 0;
        int lastDist = Integer.MAX_VALUE;
        int previousDist = Integer.MAX_VALUE;
        int currentPoint = startPoint;
        int lastPoint = 0;
        int minDist = Integer.MAX_VALUE;
        for (int i = 0; i < maxNumber; i++) {
            int dist = calculateDistances2(input, frequency, currentPoint);
            if (dist < minDist) {
                minDist = dist;
            }
            currentPoint++;
//            if(lastDist > dist) {
//                currentPoint++;
//                lastDist = dist;
//            } else {
//                return dist;
//            }
        }
        return minDist;
    }

    private int calculateDistances2(List<Integer> input, Map<Integer, Integer> frequency, int toNumber) {
        List<Integer> distances = new ArrayList<>();
        int lastDist = input.get(0) - toNumber;
        int dist = 0;
        int totalDist = 0;
        for (Integer freqKey : frequency.keySet()) {
            int steps = Math.abs(freqKey - toNumber) + 1;
            int stepDistance = IntStream.range(1, steps).boxed().mapToInt(x -> x).sum();
            dist = Math.abs(stepDistance * frequency.get(freqKey));
            totalDist += dist;
//            if(lastDist < dist) {
//                break;
//            }
//            lastDist = dist;
        }
        return totalDist;
    }

    private int tryToFindMinDist(List<Integer> input, Map<Integer, Integer> frequency) {
//        int startPoint = indexOfMaximumFrequency;
        int startPoint = 0;
        int lastDist = Integer.MAX_VALUE;
        int previousDist = Integer.MAX_VALUE;
        int currentPoint = startPoint;
        int lastPoint = 0;
        int minDist = Integer.MAX_VALUE;
        for (int i = 0; i < maxNumber; i++) {
            int dist = calculateDistances(input, frequency, currentPoint);
            if (dist < minDist) {
                minDist = dist;
            }
            currentPoint++;
//            if(lastDist > dist) {
//                currentPoint++;
//                lastDist = dist;
//            } else {
//                return dist;
//            }
        }
        return minDist;
    }

    private int calculateDistances(List<Integer> input, Map<Integer, Integer> frequency, int toNumber) {
        List<Integer> distances = new ArrayList<>();
        int lastDist = input.get(0) - toNumber;
        int dist = 0;
        int totalDist = 0;
        for (Integer freqKey : frequency.keySet()) {
            dist = Math.abs((freqKey - toNumber) * frequency.get(freqKey));
            totalDist += dist;
//            if(lastDist < dist) {
//                break;
//            }
//            lastDist = dist;
        }
        return totalDist;
    }

    public Map<Integer, Integer> countFreq(List<Integer> list) {
        Map<Integer, Integer> frequency = new HashMap();

        for (int i = 0; i < list.size(); i++) {
            int newVal = 1;
            if (frequency.containsKey(list.get(i))) {
                newVal = frequency.get(list.get(i)) + 1;
            }
            frequency.put(list.get(i), newVal);
        }
        return frequency;
    }
}