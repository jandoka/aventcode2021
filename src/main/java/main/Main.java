package main;

import main.day01.Day01Calculate;
import main.day01.ReadInputs;
import main.day02.Day02MovementCalculation;
import main.day08.Day08Calculation;

import java.io.IOException;
import java.util.List;

public class Main {

    private static final String BASE_PATH = "src/main/resources/";

    public static void main(String[] args) throws IOException {
        ReadInputs readInputs = new ReadInputs();
//        day01(readInputs);
//        runDay("day02", new Day02Calculation());
//        runDay("day03", new Day03Calculation());
//        runDay("day04", new Day04Calculation());
//        runDay("day05", new Day05Calculation());
//        runDay("day06", new Day06Calculation());
//        runDay("day07", new Day07Calculation());
        runDay("day08", new Day08Calculation());
//        runDay("day09", new Day09Calculation());
    }

    private static void runDay(String day, AdventDay calculation) throws IOException {
        System.out.println("Calculation 1:");
        System.out.println("Example:");
        calculation.calculate1(BASE_PATH + day + "/example.txt");
        System.out.println("...........................................................................................................................................................................................................................................");
        System.out.println("Input:");
        calculation.calculate1(BASE_PATH + day + "/input.txt");
        System.out.println("===========================================================================================================================================================================================================================================");
        System.out.println("===========================================================================================================================================================================================================================================");
        System.out.println("Calculation 2:");
        System.out.println("Example:");
        calculation.calculate2(BASE_PATH + day + "/example.txt");
        System.out.println("...........................................................................................................................................................................................................................................");
        System.out.println("Input:");
        calculation.calculate2(BASE_PATH + day + "/input.txt");

    }

    private static void day02() throws IOException {
        Day02MovementCalculation calculation = new Day02MovementCalculation();
        //a
        calculation.finalMovement("day02/example.txt");
        calculation.finalMovement("day02/input.txt");
        //b
        calculation.finalMovementWithAim("day02/example.txt");
        calculation.finalMovementWithAim("day02/input.txt");
    }

    private static void day01(ReadInputs readInputs) throws IOException {
        List<Integer> inputs = readInputs.readIntegerInput("day01/input.txt");
//        List<Integer> inputs = readInputs.readInput("smallExample.txt");

        Day01Calculate day01Calculate = new Day01Calculate();
        int numberOfMeasurements = day01Calculate.calucateNumberOfIncreasingMeasurements(inputs);
        System.out.println("Result 01-a: " + numberOfMeasurements);

        System.out.println("Result 01-b: " + day01Calculate.calculateNumberOfSumOfIncreasingMeas(inputs));
    }
}
