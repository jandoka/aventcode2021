package main.day03;

import com.google.common.primitives.Ints;
import main.AdventDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day03Calculation implements AdventDay {

    public void calculate1(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            List<String> lines = reader.lines().collect(Collectors.toList());
            int[] gamma = findMostCommonBits(getBitsInLines(lines));
            int[] epsilon = oppositeArray(gamma);
            System.out.println("gamma binary: " + Arrays.toString(gamma) + ", epsilon binary: " + Arrays.toString(epsilon));
            int gammaFinal = binaryToDecimal(gamma);
            int epsilonFinal = binaryToDecimal(epsilon);
            System.out.println("gamma: " + gammaFinal + ", epsilon: " + epsilonFinal + ", result: " + gammaFinal * epsilonFinal);
        }
    }

    public void calculate2(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            List<String> lines = reader.lines().collect(Collectors.toList());
            int oxygen = getOxygen(getBitsInLines(new ArrayList<>(lines)));
            int co2 = getCo2(getBitsInLines(new ArrayList<>(lines)));
            System.out.println("oxygen: " + oxygen + ", co2: " + co2 + ", result: " + oxygen * co2);
        }
    }

    private int getOxygen(List<List<Integer>> bitsInLines) {
        int numOfBitsInLine = bitsInLines.get(0).size();
        int[] mostCommonBits = findMostCommonBits(bitsInLines);
        for (int i = 0; i < numOfBitsInLine; i++) {
            if(bitsInLines.size() == 1) {
                break;
            }
            int finalI = i;
            int[] finalMostCommonBits = mostCommonBits;
            bitsInLines = bitsInLines.stream()
                    .filter(line -> line.get(finalI) == finalMostCommonBits[finalI])
                    .collect(Collectors.toList());
            mostCommonBits = findMostCommonBits(bitsInLines);
        }
        System.out.println("Oxygen binary: " + bitsInLines.get(0));
        return binaryToDecimal(Ints.toArray(bitsInLines.get(0)));
    }

    private int getCo2(List<List<Integer>> bitsInLines) {
        int numOfBitsInLine = bitsInLines.get(0).size();
        int[] leastCommonBits = oppositeArray(findMostCommonBits(bitsInLines));
        for (int i = 0; i < numOfBitsInLine; i++) {
            if(bitsInLines.size() == 1) {
                break;
            }
            int finalI = i;
            int[] finalMostCommonBits = leastCommonBits;
            bitsInLines = bitsInLines.stream()
                    .filter(line -> line.get(finalI) == finalMostCommonBits[finalI])
                    .collect(Collectors.toList());
            leastCommonBits = oppositeArray(findMostCommonBits(bitsInLines));
        }
        System.out.println("CO2 binary: " + bitsInLines.get(0));
        return binaryToDecimal(Ints.toArray(bitsInLines.get(0)));
    }

    private List<List<Integer>> getBitsInLines(List<String> lines) {
        return lines.stream()
                .map(line -> Arrays.stream(line.split(""))
                        .map(Integer::valueOf)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    private int[] findMostCommonBits(List<List<Integer>> bitsInLines) {
        int[] sums = new int[bitsInLines.get(0).size()];
        bitsInLines.forEach(line -> {
            for (int i = 0; i < sums.length; i++) {
                sums[i] += line.get(i);
            }
        });
        return assignMostCommonBits(bitsInLines.size(), sums);
    }

    private int[] assignMostCommonBits(int numberOfLines, int[] sums) {
        int half = (int) Math.ceil(numberOfLines / 2.);
        int[] mostCommonBits = new int[sums.length];
        for (int i = 0; i < sums.length; i++) {
            if (half > sums[i]) {
                mostCommonBits[i] = 0;
            } else {
                mostCommonBits[i] = 1;
            }
        }
        return mostCommonBits;
    }

    private int[] oppositeArray(int[] inputArray) {
        int[] result = new int[inputArray.length];
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] == 0) {
                result[i] = 1;
            } else {
                result[i] = 0;
            }
        }
        return result;
    }

    private int binaryToDecimal(int[] binary) {
        int decimal = 0;
        int power = 0;
        for (int i = binary.length - 1; i > -1; i--) {
            decimal += binary[i] * Math.pow(2, power);
            power++;
        }
        return decimal;
    }
}
