package main.day01;

import java.util.ArrayList;
import java.util.List;

public class Day01Calculate {

    public int calucateNumberOfIncreasingMeasurements(List<Integer> inputs) {
        Integer previous = Integer.MAX_VALUE;
        int counter = 0;
        for (Integer measurement : inputs) {
            if (measurement > previous) {
                counter++;
            }
            previous = measurement;
        }
        return counter;
    }

    public int calculateNumberOfSumOfIncreasingMeas(List<Integer> inputs) {
        List<Integer> subsections = new ArrayList<>();
        for (Integer measurement : inputs) {
            subsections.add(measurement);
            int last = subsections.size() - 1;
            if (last < 0) continue;
            if (!addSubSum(subsections, measurement, last, 1)) continue;
            if (!addSubSum(subsections, measurement, last, 2)) continue;
        }
        subsections = subsections.subList(0, subsections.size() - 2);
        return calucateNumberOfIncreasingMeasurements(subsections);
    }

    private boolean addSubSum(List<Integer> subsections, Integer measurement, int last, int i) {
        int indexOfUpdatedValue = last - i;
        if (indexOfUpdatedValue < 0) {
            return false;
        }
        int newSum = subsections.get(indexOfUpdatedValue) + measurement;
        subsections.set(indexOfUpdatedValue, newSum);
        return true;
    }
}
