package main.day01;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ReadInputs {

    public List<Integer> readIntegerInput(String fileName) throws IOException {
        String file = "src/main/resources/" + fileName;

        List<Integer> inputs = new LinkedList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String currentLine = reader.readLine();
            while (!StringUtils.isBlank(currentLine)) {
                inputs.add(Integer.valueOf(currentLine));
                currentLine = reader.readLine();
            }
        }
        return inputs;
    }
}
