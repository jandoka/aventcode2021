package main;

import java.io.IOException;

public interface AdventDay {
    public void calculate1(String file) throws IOException;
    public void calculate2(String file) throws IOException;
}
