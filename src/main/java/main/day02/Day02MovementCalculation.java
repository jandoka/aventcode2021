package main.day02;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Day02MovementCalculation {

    public int finalMovement(String filePath) throws IOException {
        String file = "src/main/resources/" + filePath;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String currentLine = reader.readLine();
            int forward = 0;
            int depth = 0;
            while (!StringUtils.isBlank(currentLine)) {
                String[] line = currentLine.split(" ");
                String direction = line[0];
                switch (direction) {
                    case "forward":
                        forward += Integer.valueOf(line[1]);
                        break;
                    case "down":
                        depth += Integer.valueOf(line[1]);
                        break;
                    case "up":
                        depth -= Integer.valueOf(line[1]);
                        break;
                }
                currentLine = reader.readLine();
            }
            System.out.println("Forward: " + forward + ", depth: " + depth + " result: " + forward * depth);
            return forward * depth;
        }
    }

    public int finalMovementWithAim(String filePath) throws IOException {
        String file = "src/main/resources/" + filePath;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String currentLine = reader.readLine();
            int horizontal = 0;
            int depth = 0;
            int aim = 0;
            while (!StringUtils.isBlank(currentLine)) {
                String[] line = currentLine.split(" ");
                String direction = line[0];
                Integer x = Integer.valueOf(line[1]);
                switch (direction) {
                    case "forward":
                        horizontal += x;
                        depth += aim*x;
                        break;
                    case "down":
                        aim += x;
                        break;
                    case "up":
                        aim -= x;
                        break;
                }
                currentLine = reader.readLine();
            }
            System.out.println("Forward: " + horizontal + ", depth: " + depth + " result: " + horizontal * depth);
            return horizontal * depth;
        }
    }
}
