package main.day05;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Board {

    private List<List<Integer>> board = new ArrayList<>();
    private final int NUM_OF_ROWS;
    private final int NUM_OF_COLS;

    public Board(int numOfRows, int numOfCols) {
        NUM_OF_ROWS = numOfRows;
        NUM_OF_COLS = numOfCols;
        initEmptyBoard();
    }

    private void initEmptyBoard() {
        for (int rowI = 0; rowI < NUM_OF_ROWS; rowI++) {
            List<Integer> row = new ArrayList<>();
            for (int colI = 0; colI < NUM_OF_COLS; colI++) {
                row.add(0);
            }
            board.add(row);
        }
    }

    public void printBoard() {
        System.out.println("-----------------------------------------------------------------------------------------------");
        for (int rowI = 0; rowI < NUM_OF_ROWS; rowI++) {
            for (int colI = 0; colI < NUM_OF_COLS; colI++) {
                if (colI != 0) {
                    System.out.print(" ");
                }
                System.out.print(board.get(rowI).get(colI));
            }
            System.out.println("");
        }
        System.out.println("-----------------------------------------------------------------------------------------------");
    }

    public void printInverseBoard() {
        System.out.println("-----------------------------------------------------------------------------------------------");
        for (int rowI = 0; rowI < NUM_OF_ROWS; rowI++) {
            for (int colI = 0; colI < NUM_OF_COLS; colI++) {
                if (colI != 0) {
                    System.out.print(" ");
                }
                System.out.print(board.get(colI).get(rowI));
            }
            System.out.println("");
        }
        System.out.println("-----------------------------------------------------------------------------------------------");
    }

    public void fillRow(int rowI, int colIFrom, int colITo) {
        if (colIFrom > colITo) {
            int tmp = colIFrom;
            colIFrom = colITo;
            colITo = tmp;
        }
        for (int colI = colIFrom; colI <= colITo; colI++) {
            addToBoard(1, rowI, colI);
        }
    }

    public void fillCol(int colI, int rowIFrom, int rowITo) {
        if (rowIFrom > rowITo) {
            int tmp = rowIFrom;
            rowIFrom = rowITo;
            rowITo = tmp;
        }
        for (int rowI = rowIFrom; rowI <= rowITo; rowI++) {
            addToBoard(1, rowI, colI);
        }
    }

    public void fillDiagonal(List<Integer> from, List<Integer> to) {
        int diff = Math.abs(from.get(0) - to.get(0));
        int[] addition = new int[2];
        if (from.get(0) < to.get(0)) {
            addition[0] = 1;
        } else {
            addition[0] = -1;
        }
        if (from.get(1) < to.get(1)) {
            addition[1] = 1;
        } else {
            addition[1] = -1;
        }

        int rowI = from.get(0);
        int colI = from.get(1);
        for (int i = 0; i <= diff; i++) {
            addToBoard(1, rowI, colI);
            rowI += addition[0];
            colI += addition[1];
        }
    }

    public void addToBoard(Integer value, int rowI, int colI) {
        Integer newValue = board.get(rowI).get(colI) + value;
        board.get(rowI).set(colI, newValue);
    }
}
