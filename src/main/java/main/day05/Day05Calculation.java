package main.day05;

import main.AdventDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day05Calculation implements AdventDay {

    public void calculate1(String file) throws IOException {
        Board board = readInputIntoFinalBoard(file, false);
//        board.printInverseBoard();
        long count = calculateMoreOverLapInBoard(1, board);
        System.out.println("result: " + count);
    }

    public void calculate2(String file) throws IOException {
        Board board = readInputIntoFinalBoard(file, true);
//        board.printInverseBoard();
        long count = calculateMoreOverLapInBoard(1, board);
        System.out.println("result: " + count);
    }

    private Board readInputIntoFinalBoard(String file, boolean takeDiagonal) throws IOException {
        Board board = initEmptyBoardFromInput(file);
        fillBoardFromInput(file, board, takeDiagonal);
        return board;
    }

    private Board initEmptyBoardFromInput(String file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            int numOfRows = 0;
            int numOfCols = 0;
            String line = reader.readLine();
            while (line != null) {
                String[] allLineCoordinates = line.split(" -> ");
                numOfRows = getMaxCoordinate(allLineCoordinates, 0, numOfRows);
                numOfCols = getMaxCoordinate(allLineCoordinates, 1, numOfCols);
                line = reader.readLine();
            }
            return new Board(numOfRows + 1, numOfCols + 1);
        }
    }

    private int getMaxCoordinate(String[] lineCoordinates, int indexCoordinate, int actualMax) {
        int newMax = Arrays.stream(lineCoordinates)
                .mapToInt(coordinates -> Integer.valueOf(coordinates.split(",")[indexCoordinate]))
                .max().orElse(0);
        if (newMax > actualMax) {
            return newMax;
        }
        return actualMax;
    }

    private void fillBoardFromInput(String file, Board board, boolean takeDiagonal) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = reader.readLine();
            while (line != null) {
                String[] allLineCoordinates = line.split(" -> ");
                fillBoard(allLineCoordinates, board, takeDiagonal);
                line = reader.readLine();
            }
        }
    }

    private void fillBoard(String[] allLineCoordinates, Board board, boolean takeDiagonal) {
        List<Integer> from = Arrays.stream(allLineCoordinates[0].split(",")).map(c -> Integer.valueOf(c)).collect(Collectors.toList());
        List<Integer> to = Arrays.stream(allLineCoordinates[1].split(",")).map(c -> Integer.valueOf(c)).collect(Collectors.toList());
        if (from.get(0).equals(to.get(0))) {
            // in one row
            board.fillRow(from.get(0), from.get(1), to.get(1));
        } else if (from.get(1).equals(to.get(1))) {
            //in one col
            board.fillCol(from.get(1), from.get(0), to.get(0));
        } else if (takeDiagonal) {
            board.fillDiagonal(from, to);
        } else {
            System.out.println("Dangerous!!! this coordinates are not in one row, neither in one column from: " + from + ", to: " + to);
        }
    }

    private long calculateMoreOverLapInBoard(int moreThanThis, Board board) {
        return board.getBoard().stream()
                .flatMapToInt(x -> x.stream().mapToInt(Integer::valueOf))
                .filter(num -> num > moreThanThis)
                .count();
    }
}