package main.day04;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Board {
    private List<List<Integer>> board = new ArrayList<>();
    private List<List<Boolean>> marked = new ArrayList<>();
    private List<Integer> numOfNonMarkedInColumns = new ArrayList<>();
    private List<Integer> numOfNonMarkedInRows = new ArrayList<>();
    private int numOfColumns = 0;
    private int numOfRows = 0;
    private Integer winRow = null;
    private Integer winCol = null;
    private List<Integer> winNumbers = new ArrayList<>();

    public void addLine(String line) {
        List<Integer> intLine = Arrays.stream(line.split(" "))
                .filter(value -> !StringUtils.isBlank(value))
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        int index = board.size();
        int sizeOfLine = intLine.size();
        board.add(intLine);
        marked.add(createMarkedLine(sizeOfLine));
        if (index == 0) {
            initNonMarkedInColumns(sizeOfLine);
            numOfColumns = sizeOfLine;
        }
        numOfNonMarkedInColumns = numOfNonMarkedInColumns.stream().map(num -> num + 1).collect(Collectors.toList());
        numOfNonMarkedInRows.add(sizeOfLine);
        numOfRows++;
    }

    private List<Boolean> createMarkedLine(int sizeOfLine) {
        ArrayList<Boolean> newLine = new ArrayList<>();
        for (int i = 0; i < sizeOfLine; i++) {
            newLine.add(false);
        }
        return newLine;
    }

    private void initNonMarkedInColumns(int sizeOfLine) {
        for (int i = 0; i < sizeOfLine; i++) {
            numOfNonMarkedInColumns.add(0);
        }
    }

    public void addNumberAndCheckIfBoardWin(Integer input) {
        for (int rowI = 0; rowI < numOfRows; rowI++) {
            for (int colI = 0; colI < numOfColumns; colI++) {
                if (board.get(rowI).get(colI).equals(input)) {
                    marked.get(rowI).set(colI, true);
                    int numInCol = numOfNonMarkedInColumns.get(colI) - 1;
                    numOfNonMarkedInColumns.set(colI, numInCol);
                    if (numInCol == 0) {
                        winCol = colI;
                    }
                    int numInRow = numOfNonMarkedInRows.get(rowI) - 1;
                    numOfNonMarkedInRows.set(rowI, numInRow);
                    if (numInRow == 0) {
                        winRow = rowI;
                    }
                }
            }
        }
    }

    public boolean isWin() {
        if (winRow != null || winCol != null) {
            return true;
        }
        return false;
    }

    public int getWinSum() {
        int sum = 0;
        for (int rowI = 0; rowI < numOfRows; rowI++) {
            for (int colI = 0; colI < numOfColumns; colI++) {
                if (!marked.get(rowI).get(colI)) {
                    sum += board.get(rowI).get(colI);
                }
            }
        }
        return sum;
    }

    private int getWinColSum() {
        int sum = 0;
        for (int rowI = 0; rowI < numOfRows; rowI++) {
            winNumbers.add(board.get(rowI).get(winCol));
            sum += board.get(rowI).get(winCol);
        }
        return sum;
    }

    private int getWinRowSum() {
        return board.get(winRow).stream().mapToInt(Integer::intValue).sum();
    }

    private int getBoardSum() {
        return board.stream()
                .flatMapToInt(row -> row.stream().mapToInt(Integer::intValue))
                .sum();
    }
}
