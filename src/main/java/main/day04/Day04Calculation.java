package main.day04;

import main.AdventDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day04Calculation implements AdventDay {

    public void calculate1(String file) throws IOException {
        List<Board> boards = new ArrayList<>();
        List<Integer> inputs = readInput(file, boards);

        Board winBoard = null;
        for (Integer input : inputs) {
            for (Board board : boards) {
                board.addNumberAndCheckIfBoardWin(input);
                if (board.isWin()) {
                    winBoard = board;
                    writeResults(winBoard, input);
                    return;
                }
            }
        }
    }

    public void calculate2(String file) throws IOException {
        List<Board> boards = new ArrayList<>();
        List<Integer> inputs = readInput(file, boards);

        Board lastBoard = null;
        int lastInput = 0;
        int numOfLoosingBoards = boards.size();
        for (int input : inputs) {
            if (numOfLoosingBoards == 0) break;
            for (Board board : boards) {
                if (numOfLoosingBoards == 0) break;
                if (board.isWin()) continue;
                board.addNumberAndCheckIfBoardWin(input);
                if (board.isWin()) {
                    lastBoard = board;
                    lastInput = input;
                    numOfLoosingBoards--;
                }
            }
        }
        writeResults(lastBoard, lastInput);
        return;
    }

    private void writeResults(Board winBoard, Integer lastInput) {
        int winSum = winBoard.getWinSum();
        int result = winSum * lastInput;
        System.out.println("Win numbers: " + winBoard.getWinNumbers());
        System.out.println("win sum: " + winSum + ", last input: " + lastInput + ", result: " + result);
    }

    private List<Integer> readInput(String file, List<Board> boards) throws IOException {
        List<Integer> inputs = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            int counter = 0;
            String currentLine = reader.readLine();
            Board board = null;
            while (currentLine != null) {
                if (counter == 0) {
                    inputs = Arrays.stream(currentLine.split(",")).map(Integer::valueOf).collect(Collectors.toList());
                } else if (currentLine.isEmpty()) {
                    if (board != null) {
                        boards.add(board);
                    }
                    board = new Board();
                } else {
                    board.addLine(currentLine);
                }
                counter++;
                currentLine = reader.readLine();
            }
            boards.add(board);
        }
        return inputs;
    }
}
